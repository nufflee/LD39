﻿using Code;
using UnityEngine;

public class PowerManager : InstancedMonoBehaviour<PowerManager>
{
    [HideInInspector] public int power;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
        if (power <= 0)
            GameController.Instance.FailGame();
    }

    public void UpdatePower(float power)
    {
        this.power = (int) power * 100;
    }
}