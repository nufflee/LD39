﻿using Code;
using UnityEngine;
using UnityEngine.EventSystems;

public class DesktopManager : MonoBehaviour, IPointerClickHandler
{
    private readonly Vector3 desiredScale = new Vector3(0.2f, 0.2f, 0.2f);
    private Vector3 startingScale;

    // Use this for initialization
    void Start()
    {
        startingScale = transform.localScale;
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        GetComponent<Animation>().Play();
        ProgressManager.Instance.AddProgress(Random.Range(1, 5));
    }
}