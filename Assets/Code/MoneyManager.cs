﻿using Code;
using UnityEngine;
using UnityEngine.UI;

public class MoneyManager : InstancedMonoBehaviour<MoneyManager>, IAcceptClicks
{
    [SerializeField] private Text moneyText;

    [HideInInspector] public int money;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void OnClick()
    {
        money += Random.Range(0, 2);
        moneyText.text = money + "$";
    }
}