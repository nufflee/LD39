﻿using UnityEngine;

namespace Code
{
    public static class Vector3Extensions
    {
        public static bool AreAllEqual(this Vector3 vector, float to)
        {
            return vector.x == to && vector.y == to && vector.z == to;
        }

        public static Vector3 AddToAll(this Vector3 vector, float number)
        {
            return new Vector3(vector.x - number, vector.y - number, vector.z - number);
        }

        public static bool LessOrEqual(this Vector3 vector, Vector3 to)
        {
            return vector.x <= to.x && vector.y <= to.y && vector.z <= to.z;
        }

        public static bool GreaterOrEqual(this Vector3 vector, Vector3 to)
        {
            return vector.x >= to.x && vector.y >= to.y && vector.z >= to.z;
        }
    }
}