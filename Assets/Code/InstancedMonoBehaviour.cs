﻿using UnityEngine;

namespace Code
{
    public class InstancedMonoBehaviour<T> : MonoBehaviour
        where T : MonoBehaviour
    {
        public static T Instance
        {
            get { return FindObjectOfType<T>(); }
        }
    }
}