﻿using UnityEngine;
using UnityEngine.UI;

namespace Code
{
    public class ProgressManager : InstancedMonoBehaviour<ProgressManager>
    {
        [SerializeField] private Slider progressStatusSlider;
        
        [HideInInspector] public int progress;

        private void Update()
        {
        }

        public void AddProgress(int progress)
        {
            this.progress += progress;
            progressStatusSlider.value += progress / 100.0f;
        }
    }
}