﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ClickManager : MonoBehaviour, IAcceptClicks
{
    [SerializeField] private GameObject powerStatusBar;

    private Slider powerStatusSlider;

    private float valueChange;
    private float difficultyMultiplier;
    private bool clickedOnce;
    private float timeSinceFirstClick;
    private float firstClickTime;
    private PowerManager powerManager;
    private int clicksPerSecond;

    private void Awake()
    {
        powerStatusSlider = powerStatusBar.GetComponent<Slider>();
        powerManager = GetComponent<PowerManager>();
    }

    // Use this for initialization
    private void Start()
    {
        StartCoroutine(CheckClicksPerSecond());
        powerStatusSlider.onValueChanged.AddListener(OnValueChanged);
    }

    // Update is called once per frame
    private void Update()
    {
        difficultyMultiplier = Time.timeSinceLevelLoad / 75;
        valueChange -= 0.005f * difficultyMultiplier;
        powerStatusSlider.value += valueChange;
        //StartCoroutine(LerpSlider(powerStatusSlider, valueChange));
        valueChange = 0;
    }

    private void OnValueChanged(float value)
    {
        //powerManager.UpdatePower(value);
    }

    private IEnumerator CheckClicksPerSecond()
    {
        while (gameObject.activeSelf)
        {
            yield return new WaitForSeconds(1);
            clicksPerSecond = 0;
        }
    }

    private static IEnumerator LerpSlider(Slider slider, float relativeValue)
    {
        float startTime = Time.time;
        float startValue = slider.value;
        float absoluteValue = startValue + relativeValue;

        while (relativeValue > 0 ? slider.value < absoluteValue : slider.value > absoluteValue)
        {
            slider.value = Mathf.Lerp(startValue, absoluteValue, (Time.time - startTime) * 10.0f);
            yield return null;
        }
    }

    public void OnClick()
    {
        clicksPerSecond++;
        valueChange += 0.1f;
    }
}